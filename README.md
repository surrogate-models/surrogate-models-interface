# Surrogate Models Interface

[![pipeline status](https://gitlab.windenergy.dtu.dk/surrogate-models/surrogate-models-interface/badges/main/pipeline.svg)](https://gitlab.windenergy.dtu.dk/surrogate-models/surrogate-models-interface/-/commits/main)
[![coverage report](https://gitlab.windenergy.dtu.dk/surrogate-models/surrogate-models-interface/badges/main/coverage.svg)](https://gitlab.windenergy.dtu.dk/surrogate-models/surrogate-models-interface/-/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Poetry](https://img.shields.io/endpoint?url=https://python-poetry.org/badge/v0.json)](https://python-poetry.org/)
[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8282614.svg)](https://doi.org/10.5281/zenodo.8282614)

Provides a common interface to several surrogate models, including an OpenMDAO wrapper.

The documentation is available at: https://surrogate-models.pages.windenergy.dtu.dk/surrogate-models-interface
