# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import surrogates_interface

sys.path.insert(0, os.path.abspath("../.."))


# -- Project information -----------------------------------------------------

project = "Surrogate Models Interface"
copyright = "2023, DTU Wind and Energy Systems"
author = "DTU Wind and Energy Systems"

# The full version, including alpha/beta/rc tags
release = surrogates_interface.__version__


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.mathjax",
    "sphinx.ext.doctest",
    "numpydoc",
    # 'sphinx.ext.autosummary',  # Loaded by numpydoc
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
    "sphinx_sitemap",
    "sphinx_design",
]

numpydoc_class_members_toctree = False

autodoc_default_options = {
    "private-members": True,
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Needed to generate the site map.
html_baseurl = "https://surrogate-models.pages.windenergy.dtu.dk/surrogate-models-interface/"
sitemap_url_scheme = "{link}"


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "pydata_sphinx_theme"

html_theme_options = {
    "logo": {
        "text": project,
       },
    "icon_links": [
        {
            # Label for this link
            "name": "GitLab",
            # URL where the link will redirect
            "url": "https://gitlab.windenergy.dtu.dk/surrogate-models/surrogate-models-interface",  # required
            # Icon class (if "type": "fontawesome"), or path to local image (if "type": "local")
            "icon": "fa-brands fa-square-gitlab",
            # The type of image to be used (see below for details)
            "type": "fontawesome",
        }
   ]
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ["_static"]

html_extra_path = ["robots.txt",
                   "search_engines_indexing",
                   ]
