.. Surrogate Models Interface documentation master file, created by
   sphinx-quickstart on Fri Mar 10 09:40:10 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

******************************************************
Welcome to Surrogate Models Interface's documentation!
******************************************************

This package provides a common interface to several surrogate models. It allows an arbitrary number of input and output transformations, created from `scikit-learn <https://scikit-learn.org/stable/index.html>`_, and includes an `OpenMDAO <https://openmdao.org/>`_ wrapper.

Supported libraries:

- `TensorFlow <https://www.tensorflow.org/>`_.
- `Surrogate Modeling Toolbox (SMT) <https://smt.readthedocs.io/en/latest/index.html>`_.
- `PyTorch <https://pytorch.org/>`_.


How to install the `surrogates_interface` package
=================================================

The `surrogates_interface` package is published on PyPI since version 4.0.1. All versions are available in the `Gitlab package registry <https://gitlab.windenergy.dtu.dk/surrogate-models/surrogate-models-interface/-/packages>`_.


Creating a virtual environment
------------------------------

Start by creating a folder for all virtual environments.

.. tab-set::

    .. tab-item:: Windows
        :sync: Windows

        .. code-block::

           mkdir %userprofile%\.venv

    .. tab-item:: Linux
        :sync: Linux

        .. code-block:: bash

           mkdir -p ~/.venv

Then, create a new one for this package.

.. tab-set::

    .. tab-item:: Windows
        :sync: Windows

        .. code-block::

           python -m venv %userprofile%\.venv\surrogates

    .. tab-item:: Linux
        :sync: Linux

        .. code-block:: bash

           python -m venv ~/.venv/surrogates


and activate it with

.. tab-set::

    .. tab-item:: Windows
        :sync: Windows

        .. code-block::

           python -m venv %userprofile%\.venv\surrogates\Scripts\activate.bat

    .. tab-item:: Linux
        :sync: Linux

        .. code-block:: bash

           source ~/.venv/surrogates/bin/activate


User installation with Pip
--------------------------

The user installation is based on pip, and there are a few options available, depending on your intended usage.

- nothing: Only the minimum set of dependencies will be installed. Not very useful.
- `smt`: Provides the Surrogate Modeling Toolbox (SMT) wrapper.
- `tf`: Provides the TensorFlow wrapper.
- `om`: Provides the OpenMDAO wrapper.
- `torch`: Provides PyTorch wrapper.
- `docs`: Allow to build the documentation via sphinx.
- `code_style`: Allow to check the code style.
- `test`: Allow to run the tests.

Install the `surrogates_interface` package with

.. code-block:: bash

   pip install surrogates_interface[<option>]

For example, to install the TensorFlow wrapper

.. code-block:: bash

   pip install surrogates_interface[tf]

Options can also be combined with

.. code-block:: bash

   pip install surrogates_interface[<option_1>,<option_2>]

For example, to install both the TensorFlow and OpenMDAO wrappers

.. code-block:: bash

   pip install surrogates_interface[tf,om]

Or, to install both the PyTorch and OpenMDAO wrappers

.. code-block:: bash

   pip install surrogates_interface[torch,om]

If you need explicit support for GPU acceleration, you have to install PyTorch seperately from https://pytorch.org/get-started/locally/.


Developer installation using Poetry
-----------------------------------

Clone the **Surrogate Models Interface** repository:

.. code-block:: bash

   git clone https://gitlab.windenergy.dtu.dk/surrogate-models/surrogate-models-interface.git
   cd surrogate-models-interface

Install Poetry.

.. code-block:: bash

   pip install poetry

Set Poetry to install the package in the activated virtual environment.

.. code-block:: bash

   poetry env use system

And install

.. code-block:: bash

   poetry install --extras "<option_1> <option_2>"

For example, to install both the TensorFlow and OpenMDAO wrappers

.. code-block:: bash

   poetry install --extras "tf om"

To install all the optional dependencies, write

.. code-block:: bash

   poetry install --all-extras


How to build the documentation
==============================

First install this package with the `docs` option, then use sphinx.

With a user installation

.. code-block:: bash

   pip install surrogates_interface[docs]
   cd docs
   make html

Or, with a developer installation

.. code-block:: bash

   poetry install --extras "docs"
   cd docs
   poetry run sphinx-build -M html source build

Finally, open `build/html/index.html`.


How to cite this package
========================

To cite this package, please add to your BibTeX file

.. code-block:: bibtex

   @software{surrogates_interface,
   author    = {Riva, Riccardo and
                Pedersen, Mads Mølgaard and
                Simutis, Ernestas},
   title     = {Surrogate Models Interface},
   month     = nov,
   year      = 2024,
   publisher = {Zenodo},
   doi       = {10.5281/zenodo.8282614},
   url       = {https://gitlab.windenergy.dtu.dk/surrogate-models/surrogate-models-interface}
   }


Publications
============

This package has been used in the following publications.

.. code-block:: bibtex

   @article{RRiva_etal_2024,
     title = {Incorporation of floater rotation and displacement in a static wind farm simulator},
     author = {Riva, Riccardo and
               Pedersen, Mads Mølgaard and
               Pirrung, Georg and
               Bredmose, Henrik and
               Feng, Ju},
     doi = {10.1088/1742-6596/2767/6/062019},
     year = {2024},
     month = {jun},
     publisher = {IOP Publishing},
     volume = {2767},
     number = {6},
     pages = {062019},
     journal = {Journal of Physics: Conference Series},
     note = {Presented at {TORQUE} 2024},
   }

   @article{JFeng_etal_2024,
     title = {Design optimization of floating offshore wind farms using a steady state movement and flow model},
     author = {Feng, Ju and
               Pedersen, Mads Mølgaard and
               Riva, Riccardo and
               Bredmose, Henrik and
               Santos, Pedro},
     doi = {10.1088/1742-6596/2875/1/012039},
     year = {2024},
     month = {nov},
     publisher = {IOP Publishing},
     volume = {2875},
     number = {1},
     pages = {012039},
     journal = {Journal of Physics: Conference Series},
     note = {Presented at {EERA DeepWind} 2024},
   }

Contents
========

.. toctree::
    :maxdepth: 3
    :titlesonly:

    user_guide/index
    api_reference/index
