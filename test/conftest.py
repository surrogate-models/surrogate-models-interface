import pytest
import os
from pathlib import Path
import uuid

TMP_DIR = Path("./test/tmpfiles")


@pytest.fixture(scope="session", autouse=True)
def setup_tmpfiles():
    assert os.path.exists("./test"), "Probably running not from project root : )"
    if not os.path.exists(TMP_DIR):
        os.makedirs(TMP_DIR)
    os.environ["OPENMDAO_WORKDIR"] = "./test/tmpfiles"
    yield
    # shutil.rmtree(tmp_dir)


@pytest.fixture()
def tmp_model_data_paths():
    uid = uuid.uuid4()
    model_path = TMP_DIR / f"{uid}-model.h5"
    extra_data_path = TMP_DIR / f"{uid}-extra-data.h5"
    return (model_path, extra_data_path)


@pytest.fixture()
def tmp_dir():
    return TMP_DIR
