import torch
from surrogates_interface.surrogates import PyTorchModel, ADMode
from surrogates_interface.transformers import StandardScalerTorch
import numpy as np
import numpy.testing as npt
import pytest
from unittest import mock

torch.manual_seed(42)
np.random.seed(42)


class DummySinModel(torch.nn.Module):
    def __init__(self, input_dim=1, output_dim=1):
        super(DummySinModel, self).__init__()
        self.n_inputs = input_dim
        self.n_outputs = output_dim

    def forward(self, x):
        return torch.sin(x)


class DummyLinearModel(torch.nn.Module):

    def __init__(self, input_dim=3, output_dim=2):
        super(DummyLinearModel, self).__init__()
        self.n_inputs = input_dim
        self.n_outputs = output_dim
        self.linear = torch.nn.Linear(self.n_inputs, self.n_outputs)
        self.linear.weight = torch.nn.Parameter(torch.randn(output_dim, input_dim))
        self.linear.bias = torch.nn.Parameter(torch.randn(output_dim))

    def forward(self, x):
        return self.linear(x)

    def expected_jacobian(self, x):
        jac = torch.zeros((x.shape[0], self.n_outputs, self.n_inputs))
        for i in range(x.shape[0]):
            jac[i] = self.linear.weight.data
        return jac


class DummyNonLinearModel(torch.nn.Module):
    def __init__(self, input_dim, output_dim):
        super(DummyNonLinearModel, self).__init__()
        self.n_inputs = input_dim
        self.n_outputs = output_dim
        self.linear = torch.nn.Linear(input_dim, output_dim)

    def forward(self, x):
        return torch.tanh(self.linear(x))


@pytest.mark.parametrize("batch_size", [None, 1, 10])
def test_inference_batch_sizes(batch_size):
    sin_model = DummySinModel(1, 1)
    bs = batch_size
    pt_surr = PyTorchModel(
        sin_model,
        n_inputs=sin_model.n_inputs,
        n_outputs=sin_model.n_outputs,
        dtype=torch.float32,
    )
    if bs is None:
        x = np.zeros((sin_model.n_inputs))
        expected_output = np.zeros((1, sin_model.n_outputs))
    else:
        x = np.zeros((bs, sin_model.n_inputs))
        expected_output = np.zeros((bs, sin_model.n_outputs))
    y = pt_surr.predict_output(x)
    npt.assert_allclose(y, expected_output)

    x = np.ones((batch_size or 1, sin_model.n_inputs)) * (np.pi / 2)
    y = pt_surr.predict_output(x)
    npt.assert_allclose(y, np.ones((batch_size or 1, sin_model.n_inputs)))


@pytest.mark.parametrize(
    "model",
    [
        DummyLinearModel(1, 1),  # SISO
        DummyLinearModel(1, 2),  # SIMO
        DummyLinearModel(1, 3),  # SIMO
        DummyLinearModel(2, 1),  # MISO
        DummyLinearModel(3, 1),  # MISO
        DummyLinearModel(2, 2),  # MIMO
        DummyLinearModel(3, 4),  # MIMO
    ],
)
def test_inference_linear_model(model):
    dummy_linear_model = model
    batch_size = 10
    inp_size = dummy_linear_model.n_inputs
    out_size = dummy_linear_model.n_outputs
    pt_surr = PyTorchModel(
        dummy_linear_model,
        n_inputs=inp_size,
        n_outputs=out_size,
        dtype=torch.float64,
    )
    x = np.random.randn(batch_size, inp_size)
    y = pt_surr.predict_output(x)
    npt.assert_allclose(
        y,
        np.dot(x, dummy_linear_model.linear.weight.data.numpy().T)
        + dummy_linear_model.linear.bias.data.numpy(),
    )


def test_inference_with_transform():
    dummy_sin_model = DummySinModel()
    scale = 1.1
    mean = 2.0
    scaler = StandardScalerTorch(scale, mean)
    inp_size = 1
    out_size = inp_size
    batch_size = 1
    pt_surr = PyTorchModel(
        dummy_sin_model,
        n_inputs=inp_size,
        n_outputs=out_size,
        input_transformers=[scaler],
        output_transformers=[scaler],
        dtype=torch.float32,
    )
    x = np.ones((batch_size, inp_size)) * (np.pi / 2)
    y = pt_surr.predict_output(x)
    expected_output = (x - mean) / scale
    expected_output = np.sin(expected_output)
    expected_output = expected_output * scale + mean
    npt.assert_allclose(y, expected_output)


@pytest.mark.parametrize(
    "model",
    [
        DummyLinearModel(),
        DummySinModel(),
        DummyNonLinearModel(64, 31),
    ],
)
def test_jacobian_rev_fwd(model):
    batch_size = 64
    for dtype, tol in zip([torch.float16, torch.float32], [1e-4, 1e-6]):
        pt_surr = PyTorchModel(
            model,
            n_inputs=model.n_inputs,
            n_outputs=model.n_outputs,
            dtype=dtype,
        )
        x = np.random.randn(batch_size, model.n_inputs)
        _, jac_fwd = pt_surr.predict_output_and_jacobian(x, ad_mode=ADMode.FWD)
        _, jac_rev = pt_surr.predict_output_and_jacobian(x, ad_mode=ADMode.REV)
        npt.assert_allclose(jac_fwd, jac_rev, atol=tol)


def test_jacobian_ad_mode_choosen_correct():
    batch_size = 32

    # input_dim < output_dim
    model = DummyNonLinearModel(31, 64)
    pt_surr = PyTorchModel(
        model,
        n_inputs=model.n_inputs,
        n_outputs=model.n_outputs,
        dtype=torch.float32,
    )
    x = np.random.randn(batch_size, model.n_inputs)

    # check if forward mode is selected
    with mock.patch.object(
        pt_surr,
        "_predict_model_output_and_jacobian_fwd",
        side_effect=pt_surr._predict_model_output_and_jacobian_fwd,
    ) as mock_fwd:
        _ = pt_surr.predict_output_and_jacobian(x)
        assert mock_fwd.call_count == 1

    # input_dim > output_dim
    model = DummyNonLinearModel(64, 31)
    pt_surr = PyTorchModel(
        model,
        n_inputs=model.n_inputs,
        n_outputs=model.n_outputs,
        dtype=torch.float32,
    )
    x = np.random.randn(batch_size, model.n_inputs)

    # check if reverse mode is selected
    with mock.patch.object(
        pt_surr,
        "_predict_model_output_and_jacobian_rev",
        side_effect=pt_surr._predict_model_output_and_jacobian_rev,
    ) as mock_rev:
        _ = pt_surr.predict_output_and_jacobian(x)
        assert mock_rev.call_count == 1


@pytest.mark.parametrize("ad_mode", [ADMode.FWD, ADMode.REV])
@pytest.mark.parametrize("batch_size", [None, 1, 10, 50_001])
def test_jacobian_batch_size(ad_mode, batch_size):
    dummy_sin_model = DummySinModel()
    inp_size = dummy_sin_model.n_inputs
    out_size = dummy_sin_model.n_inputs
    pt_surr = PyTorchModel(
        dummy_sin_model,
        n_inputs=inp_size,
        n_outputs=out_size,
    )
    x = np.zeros((batch_size, inp_size) if batch_size else (inp_size,))
    _, jac = pt_surr.predict_output_and_jacobian(x, ad_mode=ad_mode)
    expected_jac = np.cos(np.zeros((batch_size or 1, out_size, inp_size)))
    npt.assert_equal(jac.shape, expected_jac.shape)
    npt.assert_allclose(jac, expected_jac)


@pytest.mark.parametrize("ad_mode", [ADMode.FWD, ADMode.REV])
@pytest.mark.parametrize(
    "model",
    [
        DummyLinearModel(1, 1),  # SISO
        DummyLinearModel(1, 2),  # SIMO
        DummyLinearModel(1, 3),  # SIMO
        DummyLinearModel(2, 1),  # MISO
        DummyLinearModel(3, 1),  # MISO
        DummyLinearModel(2, 2),  # MIMO
        DummyLinearModel(3, 4),  # MIMO
    ],
)
def test_jacobian_linear_model(ad_mode, model):
    dummy_linear_model = model
    batch_size = 10
    inp_size = dummy_linear_model.n_inputs
    out_size = dummy_linear_model.n_outputs
    pt_surr = PyTorchModel(
        dummy_linear_model,
        n_inputs=inp_size,
        n_outputs=out_size,
    )
    x = np.random.randn(batch_size, inp_size)
    _, jac = pt_surr.predict_output_and_jacobian(x, ad_mode=ad_mode)
    expected_jac = dummy_linear_model.expected_jacobian(x)
    npt.assert_equal(jac.shape, expected_jac.shape)
    npt.assert_allclose(jac, expected_jac)


@pytest.mark.parametrize("ad_mode", [ADMode.FWD, ADMode.REV])
def test_jacobian_with_transform(ad_mode):
    dummy_sin_model = DummySinModel()
    scale = 1.1
    mean = 2.0
    scaler = StandardScalerTorch(scale, mean)
    inp_size = 1
    out_size = inp_size
    batch_size = 1
    pt_surr = PyTorchModel(
        dummy_sin_model,
        n_inputs=inp_size,
        n_outputs=out_size,
        input_transformers=[scaler],
        output_transformers=[scaler],
        dtype=torch.float32,
    )
    x = np.ones((batch_size, inp_size)) * (np.pi / 2)
    _, jac = pt_surr.predict_output_and_jacobian(x, ad_mode=ad_mode)

    input_to_model = (x - mean) / scale

    model_jac = np.cos(input_to_model)
    tf_grad = scaler.transform_gradient(x)
    inv_tf_grad = scaler.inverse_transform_gradient(x)

    expected_jac = np.zeros((batch_size, out_size, inp_size))
    tmp_jac = tf_grad
    tmp_jac = model_jac @ tmp_jac
    tmp_jac = inv_tf_grad @ tmp_jac
    expected_jac[0] = tmp_jac

    npt.assert_allclose(jac, expected_jac, atol=1e-5)


def test_sdsc_torch_eq():
    assert StandardScalerTorch(1, 1) == StandardScalerTorch(1, 1)
    assert StandardScalerTorch(1, 0.1) != StandardScalerTorch(1, 1)
    assert StandardScalerTorch(0.1, 0.1) != None


def test_save_load_model(tmp_dir):
    dummy_linear_model = DummyLinearModel()
    batch_size = 10
    inp_size = dummy_linear_model.n_inputs
    out_size = dummy_linear_model.n_outputs
    pt_surr = PyTorchModel(
        dummy_linear_model,
        n_inputs=inp_size,
        n_outputs=out_size,
    )
    x = np.random.rand(batch_size, inp_size)
    y_org = pt_surr.predict_output(x)

    path = tmp_dir / "test_model.pt"
    # remove if file already exists
    if path.exists():
        path.unlink()
    pt_surr._save_model(path)
    assert path.exists(), "Failed to save model.."

    loaded_model = PyTorchModel._load_model(path, dummy_linear_model)
    pt_surr_loaded = PyTorchModel(
        loaded_model,
        n_inputs=inp_size,
        n_outputs=out_size,
    )
    y_loaded = pt_surr_loaded.predict_output(x)

    npt.assert_allclose(y_org, y_loaded)


@pytest.mark.parametrize("ad_mode", [ADMode.FWD, ADMode.REV])
def test_jvp_func(ad_mode):
    dummy_linear_model = DummyLinearModel()
    batch_size = 10
    inp_size = dummy_linear_model.n_inputs
    out_size = dummy_linear_model.n_outputs
    pt_surr = PyTorchModel(
        dummy_linear_model,
        n_inputs=inp_size,
        n_outputs=out_size,
    )
    x = np.random.randn(batch_size, inp_size)
    _, jac = pt_surr.predict_output_and_jacobian(x, ad_mode=ad_mode)
    for idx in range(batch_size):
        v = np.zeros_like(x)
        v[idx, 0] = 1
        jvp = pt_surr.predict_jvp(x, v)
        jvp_approx = np.dot(jac, v.T).transpose(0, 2, 1)[idx]
        npt.assert_allclose(jvp, jvp_approx)


@pytest.mark.parametrize("ad_mode", [ADMode.FWD, ADMode.REV])
def test_vjp_func(ad_mode):
    dummy_linear_model = DummyLinearModel()
    batch_size = 10
    inp_size = dummy_linear_model.n_inputs
    out_size = dummy_linear_model.n_outputs
    pt_surr = PyTorchModel(
        dummy_linear_model,
        n_inputs=inp_size,
        n_outputs=out_size,
    )
    x = np.random.randn(batch_size, inp_size)
    __y = pt_surr.predict_output(x)
    _, jac = pt_surr.predict_output_and_jacobian(x, ad_mode=ad_mode)
    for idx in range(batch_size):
        v = np.zeros_like(__y)
        v[idx, 0] = 1
        v_single = v[idx]
        jac_single = jac[idx]
        vjp = pt_surr.predict_vjp(x[idx : idx + 1], v_single[np.newaxis, :])
        vjp_approx = np.dot(v_single, jac_single)
        vjp = vjp[0]  # single point
        npt.assert_allclose(vjp, vjp_approx)
